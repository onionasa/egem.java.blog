package net.egemsoft.repositories;

import net.egemsoft.models.RelTestModel;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * Created by orionasa on 23.07.2015.
 */
@Repository
public interface RelRepo extends JpaRepository<RelTestModel,Integer> {
}
