package net.egemsoft.repositories;

import net.egemsoft.models.PostModel;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Created by orionasa on 24.07.2015.
 */
public interface PostDao extends JpaRepository<PostModel,Integer> {
}
