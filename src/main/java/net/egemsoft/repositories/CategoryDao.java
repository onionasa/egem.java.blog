package net.egemsoft.repositories;

import net.egemsoft.models.CategoryModel;
import net.egemsoft.models.UserModel;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

/**
 * Created by orionasa on 24.07.2015.
 */
public interface CategoryDao extends JpaRepository<CategoryModel,Integer>{
    List<CategoryModel> findByCatUser(UserModel userModel);
}
