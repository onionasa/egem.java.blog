package net.egemsoft.repositories;

import net.egemsoft.models.UserModel;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * Created by orionasa on 24.07.2015.
 */
@Repository
public interface UserDao extends JpaRepository<UserModel,Integer> {

    UserModel findByUsername(String username);
}
