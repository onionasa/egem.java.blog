package net.egemsoft.repositories;

import net.egemsoft.models.TestModel;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * Created by orionasa on 22.07.2015.
 */
@Repository
public interface TestRepo extends JpaRepository<TestModel, Integer> {
    TestModel findByText(String text);
}