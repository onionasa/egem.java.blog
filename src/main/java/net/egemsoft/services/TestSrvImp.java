package net.egemsoft.services;

import net.egemsoft.models.TestModel;
import net.egemsoft.repositories.TestRepo;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.List;

/**
 * Created by orionasa on 22.07.2015.
 */
@Component
public class TestSrvImp implements TestSrv {

    @Resource
    private TestRepo testRepo;

    

    @Override
    @Transactional
    public TestModel create(TestModel tm) {
        return testRepo.save(tm);
    }

    @Override
    @Transactional
    public List<TestModel> findAll() {
        return testRepo.findAll();
    }

    @Override
    @Transactional
    public TestModel findById(int id) {
        return testRepo.findOne(id);
    }

    @Override
    @Transactional
    public TestModel findByText(String s) {
        return testRepo.findByText(s);
    }


}
