package net.egemsoft.services;

import net.egemsoft.models.RelTestModel;


import java.util.List;

/**
 * Created by orionasa on 23.07.2015.
 */
public interface RelSrv {
    public RelTestModel create(RelTestModel rtm);
    public List<RelTestModel> findAll();
    public RelTestModel findById(int id);
}
