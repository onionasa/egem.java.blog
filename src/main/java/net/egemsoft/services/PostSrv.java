package net.egemsoft.services;

import net.egemsoft.models.CategoryModel;
import net.egemsoft.models.PostModel;
import net.egemsoft.models.UserModel;

import java.util.List;

/**
 * Created by orionasa on 24.07.2015.
 */
public interface PostSrv {
    public PostModel create(PostModel postModel);
    public PostModel delete(int id);
    public List<PostModel> findAll();
    public PostModel update(PostModel postModel) throws Exception;
    public PostModel findById(int id);
}
