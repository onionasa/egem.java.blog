package net.egemsoft.services;

import net.egemsoft.models.UserModel;
import net.egemsoft.repositories.UserDao;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.List;

/**
 * Created by orionasa on 24.07.2015.
 */
@Component
public class UserSrvImp implements UserSrv {
    @Resource
    private UserDao userDao;

    @Override
    @Transactional
    public UserModel create(UserModel userModel) {
        return userDao.save(userModel);
    }

    @Override
    @Transactional
    public UserModel delete(int id) {
        UserModel userModel = userDao.findOne(id);
        userDao.delete(id);
        return userModel;
    }

    @Override
    @Transactional
    public List<UserModel> findAll() {
        return userDao.findAll();
    }

    @Override
    @Transactional
    public UserModel update(UserModel userModel) throws Exception {
        UserModel updatedUser = userDao.findOne(userModel.getId());
        if(updatedUser == null){
            throw  new Exception();
        }
        updatedUser.setUsername(userModel.getUsername());
        updatedUser.setPassword(userModel.getPassword());
        updatedUser.setMail(userModel.getMail());
        updatedUser.setRole(userModel.getRole());
        updatedUser.setFirst_name(userModel.getFirst_name());
        updatedUser.setLast_name(userModel.getLast_name());
        return updatedUser;
    }

    @Override
    @Transactional
    public UserModel findById(int id) {
        return userDao.findOne(id);
    }

    @Override
    public UserModel findByUsername(String username) {
        return userDao.findByUsername(username);
    }
}
