package net.egemsoft.services;

import net.egemsoft.models.PostModel;
import net.egemsoft.repositories.PostDao;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.List;

/**
 * Created by orionasa on 24.07.2015.
 */
@Component
public class PostSrvImp implements PostSrv {
    @Resource
    private PostDao postDao;

    @Override
    @Transactional
    public PostModel create(PostModel postModel) {
        return postDao.save(postModel);
    }

    @Override
    @Transactional
    public PostModel delete(int id) {
        PostModel postModel = postDao.findOne(id);
        postDao.delete(id);
        return postModel;
    }

    @Override
    @Transactional
    public List<PostModel> findAll() {
        return postDao.findAll();
    }

    @Override
    @Transactional
    public PostModel update(PostModel postModel) throws Exception {
        PostModel updatedPost = postDao.findOne(postModel.getId());
        if(updatedPost == null){
            throw  new Exception();
        }

        postDao.save(updatedPost);
        return updatedPost;
    }

    @Override
    @Transactional
    public PostModel findById(int id) {
        return null;
    }
}
