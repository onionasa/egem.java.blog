package net.egemsoft.services;

import net.egemsoft.models.UserModel;

import java.util.List;

/**
 * Created by orionasa on 24.07.2015.
 */
public interface UserSrv {
    public UserModel create(UserModel shop);
    public UserModel delete(int id);
    public List<UserModel> findAll();
    public UserModel update(UserModel shop) throws Exception;
    public UserModel findById(int id);
    public UserModel findByUsername(String username);
}
