package net.egemsoft.services;

import net.egemsoft.models.TestModel;

import java.util.List;

/**
 * Created by orionasa on 22.07.2015.
 */
public interface TestSrv {

    public TestModel create(TestModel tm);
    public List<TestModel> findAll();
    public TestModel findById(int id);
    public TestModel findByText(String s);

}