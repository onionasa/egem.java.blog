package net.egemsoft.services;

import net.egemsoft.models.CategoryModel;
import net.egemsoft.models.UserModel;
import net.egemsoft.repositories.CategoryDao;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.List;

/**
 * Created by orionasa on 24.07.2015.
 */
@Component
public class CategorySrvImp implements CategorySrv {
    @Resource
    private CategoryDao categoryDao;

    @Override
    @Transactional
    public CategoryModel create(CategoryModel categoryModel) {
        categoryModel.setCat_slug(categoryModel.getCat_name().toLowerCase().replace(" ","_"));
        return categoryDao.save(categoryModel);
    }

    @Override
    @Transactional
    public CategoryModel delete(int id) {
        CategoryModel categoryModel = categoryDao.findOne(id);
        categoryDao.delete(id);
        return categoryModel;
    }

    @Override
    @Transactional
    public List<CategoryModel> findAll() {
        return categoryDao.findAll();
    }

    @Override
    @Transactional
    public List<CategoryModel> findByCat_User(UserModel id) {
        return categoryDao.findByCatUser(id);
    }

    @Override
    @Transactional
    public CategoryModel update(CategoryModel categoryModel) throws Exception {
        CategoryModel updatedCat = categoryDao.findOne(categoryModel.getId());
        if(updatedCat == null){
            throw  new Exception();
        }
        updatedCat.setCat_name(categoryModel.getCat_name());
        updatedCat.setCat_slug(categoryModel.getCat_name().toLowerCase().replace(" ","_"));
        categoryDao.save(updatedCat);
        return updatedCat;
    }

    @Override
    @Transactional
    public CategoryModel findById(int id) {
        return categoryDao.findOne(id);
    }
}
