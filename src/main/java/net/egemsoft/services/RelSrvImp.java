package net.egemsoft.services;

import net.egemsoft.models.RelTestModel;
import net.egemsoft.repositories.RelRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.List;

/**
 * Created by orionasa on 23.07.2015.
 */
@Component
public class RelSrvImp implements RelSrv {

    @Resource
    private RelRepo relRepo;

    @Override
    @Transactional
    public RelTestModel create(RelTestModel rtm) {
        RelTestModel rtM = relRepo.save(rtm);
        rtM.setTm(rtm.getTm());
        return rtM;
    }

    @Override
    public List<RelTestModel> findAll() {
        return relRepo.findAll();
    }

    @Override
    public RelTestModel findById(int id) {
        return relRepo.findOne(id);
    }
}
