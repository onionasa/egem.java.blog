package net.egemsoft.services;

import net.egemsoft.models.CategoryModel;
import net.egemsoft.models.UserModel;

import java.util.List;

/**
 * Created by orionasa on 24.07.2015.
 */
public interface CategorySrv {
    public CategoryModel create(CategoryModel categoryModel);
    public CategoryModel delete(int id);
    public List<CategoryModel> findAll();
    public List<CategoryModel> findByCat_User(UserModel id);
    public CategoryModel update(CategoryModel shop) throws Exception;
    public CategoryModel findById(int id);
}
