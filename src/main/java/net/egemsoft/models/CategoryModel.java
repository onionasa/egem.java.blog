package net.egemsoft.models;

import javax.persistence.*;

/**
 * Created by orionasa on 24.07.2015.
 */
@Entity
@Table(name = "categorymodel")
public class CategoryModel {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int id;
    private String cat_name;
    private String cat_slug;
    @ManyToOne(optional = false)
    @JoinColumn(name = "cat_user", referencedColumnName = "id")
    private UserModel catUser;

    public CategoryModel(String cat_name, String cat_slug, UserModel cat_user) {
        this.cat_name = cat_name;
        this.cat_slug = cat_slug;
        this.catUser = cat_user;
    }

    public CategoryModel(String cat_name, UserModel cat_user) {
        this.cat_name = cat_name;
        this.catUser = cat_user;
    }

    public CategoryModel() {

    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getCat_name() {
        return cat_name;
    }

    public void setCat_name(String cat_name) {
        this.cat_name = cat_name;
    }

    public String getCat_slug() {
        return cat_slug;
    }

    public void setCat_slug(String cat_slug) {
        this.cat_slug = cat_slug;
    }

    public UserModel getCatUser() {
        return catUser;
    }

    public void setCatUser(UserModel catUser) {
        this.catUser = catUser;
    }
}
