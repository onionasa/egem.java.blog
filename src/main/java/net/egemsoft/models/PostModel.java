package net.egemsoft.models;

import org.springframework.data.annotation.CreatedDate;

import javax.persistence.*;
import java.util.Date;

/**
 * Created by orionasa on 24.07.2015.
 */
@Entity
public class PostModel {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int id;
    private String postTitle;
    private String postContent;
    private int postLike;
    private int postDislike;
    @CreatedDate
    @Column(nullable = true)
    private Date createDate;
    @ManyToOne(optional = false)
    @JoinColumn(name = "postUser", referencedColumnName = "id")
    private UserModel postUser;

    public PostModel(UserModel postUser, String postContent, String postTitle) {
        this.postUser = postUser;
        this.postContent = postContent;
        this.postTitle = postTitle;
    }

    public PostModel() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getPostTitle() {
        return postTitle;
    }

    public void setPostTitle(String postTitle) {
        this.postTitle = postTitle;
    }

    public String getPostContent() {
        return postContent;
    }

    public void setPostContent(String postContent) {
        this.postContent = postContent;
    }

    public int getPostLike() {
        return postLike;
    }

    public void setPostLike(int postLike) {
        this.postLike = postLike;
    }

    public int getPostDislike() {
        return postDislike;
    }

    public void setPostDislike(int postDislike) {
        this.postDislike = postDislike;
    }

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    public UserModel getPostUser() {
        return postUser;
    }

    public void setPostUser(UserModel postUser) {
        this.postUser = postUser;
    }
}
