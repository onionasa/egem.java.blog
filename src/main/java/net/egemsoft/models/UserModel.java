package net.egemsoft.models;

import org.codehaus.jackson.annotate.JsonIgnore;

import javax.persistence.*;

/**
 * Created by orionasa on 24.07.2015.
 */

@Entity
public class UserModel {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int id;


    public UserModel(String username, String password, String role, String mail, String first_name, String last_name) {
        this.username = username;
        this.password = password;
        this.role = role;
        this.mail = mail;
        this.first_name = first_name;
        this.last_name = last_name;
    }

    public UserModel(String username, String password, String role, String mail) {
        this.username = username;
        this.password = password;
        this.role = role;
        this.mail = mail;
    }
    public UserModel(){

    }
    private String username;
    private String password;
    @Column(name = "role", nullable = true)
    private String role;
    private String mail;
    @Column(name = "first_name", nullable = true)
    private String first_name;
    @Column(name = "last_name", nullable = true)
    private String last_name;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    @JsonIgnore
    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    public String getMail() {
        return mail;
    }

    public void setMail(String mail) {
        this.mail = mail;
    }

    public String getFirst_name() {
        return first_name;
    }

    public void setFirst_name(String first_name) {
        this.first_name = first_name;
    }

    public String getLast_name() {
        return last_name;
    }

    public void setLast_name(String last_name) {
        this.last_name = last_name;
    }
}
