package net.egemsoft.models;

import javax.persistence.*;

/**
 * Created by orionasa on 23.07.2015.
 */
@Entity
public class RelTestModel {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int id;
    @ManyToOne(optional=false)
    @JoinColumn(name="TM_ID",referencedColumnName="id")
    private TestModel tm;
    private String mext;


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public TestModel getTm() {
        return tm;
    }


    public void setTm(TestModel tm) {
        this.tm = tm;
    }

    public String getMext() {
        return mext;
    }

    public void setMext(String mext) {
        this.mext = mext;
    }
}
