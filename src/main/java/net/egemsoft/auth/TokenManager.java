package net.egemsoft.auth;

import org.apache.commons.codec.binary.Base64;
import javax.servlet.http.HttpServletRequest;
import java.nio.charset.StandardCharsets;
import java.util.HashMap;


/**
 * Created by orionasa on 23.07.2015.
 */
public class TokenManager {

    private HashMap<String,String> tokenList = new HashMap<String, String>();

    private static TokenManager instance = null;

    public String setToken(String user,HttpServletRequest request){

        byte[] encodedBytes = Base64.encodeBase64((user+request.getSession().getId()).getBytes());
        String str = new String(encodedBytes, StandardCharsets.UTF_8);
        tokenList.put(str,user);
        return str;
    }

    public HashMap<String,String> getTokenList(){
        return this.tokenList;
    }

    public boolean isValidToken(String token){
           return this.tokenList.containsKey(token);
    }

    public void removeToken(String token){
        this.tokenList.remove(token);
    }

    public String getUserToken(String user){
        for (String o : tokenList.keySet()) {
            if (tokenList.get(o).equals(user)) {
                return o;
            }
        }
        return null;
    }

    public static TokenManager getInstance() {
        if(instance == null) {
            instance = new TokenManager();
        }
        return instance;
    }
    public String getCurrentUser(String token){
        return tokenList.get(token);
    }

}
