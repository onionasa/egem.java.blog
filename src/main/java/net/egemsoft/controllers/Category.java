package net.egemsoft.controllers;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.servlet.http.HttpServletResponse;
import net.egemsoft.auth.TokenManager;
import net.egemsoft.models.CategoryModel;
import net.egemsoft.services.CategorySrv;
import net.egemsoft.services.UserSrv;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping({"/category"})
public class Category {
    @Autowired
    private CategorySrv categorySrv;
    @Autowired
    private UserSrv userSrv;

    public Category() {
    }

    @RequestMapping(
            method = {RequestMethod.POST}
    )
    @ResponseBody
    public HashMap<String, Object> createCategory(@RequestHeader("TOK") String token, @RequestBody Map<String, Object> request, HttpServletResponse response) {
        HashMap hm = new HashMap();
        TokenManager tm = TokenManager.getInstance();
        if(request == null) {
            hm.put("info", "parameters can not be null");
            return hm;
        } else if(tm.isValidToken(token)) {
            CategoryModel categoryModel = new CategoryModel(request.get("cat_name").toString(), this.userSrv.findByUsername(tm.getCurrentUser(token)));
            this.categorySrv.create(categoryModel);
            hm.put("info", "category successfully created " + categoryModel.getCat_name());
            return hm;
        } else {
            hm.put("info", "invalid token");
            return hm;
        }
    }

    @RequestMapping(value = "{id}",
            method = {RequestMethod.PUT}
    )
    @ResponseBody
    public HashMap<String, Object> updateCategory(@PathVariable int id,@RequestHeader("TOK") String token, @RequestBody Map<String, Object> request, HttpServletResponse response) throws Exception {
        HashMap hm = new HashMap();
        TokenManager tm = TokenManager.getInstance();
        if(request == null) {
            hm.put("info", "parameters can not be null");
            return hm;
        } else if(tm.isValidToken(token)) {
            CategoryModel categoryModel = categorySrv.findById(id);
            categoryModel.setCat_name(request.get("cat_name").toString());
            categoryModel.setCatUser(this.userSrv.findByUsername(tm.getCurrentUser(token)));
            categorySrv.update(categoryModel);
            hm.put("info", "category successfully updated " + categoryModel.getCat_name());
            return hm;
        } else {
            hm.put("info", "invalid token");
            return hm;
        }
    }

    @RequestMapping(
            value = {"/catList"},
            method = {RequestMethod.GET}
    )
    @ResponseBody
    public List<CategoryModel> getCategory(@RequestHeader("TOK") String token, HttpServletResponse response) {
        TokenManager tm = TokenManager.getInstance();
        return tm.isValidToken(token)?this.categorySrv.findByCat_User(this.userSrv.findByUsername(tm.getCurrentUser(token))):null;
    }

    @RequestMapping(
            value = {"/deleteCat/{id}"},
            method = {RequestMethod.GET}
    )
    @ResponseBody
    public HashMap<String, Object> deleteCategory(@PathVariable int id, @RequestHeader("TOK") String token, HttpServletResponse response) {
        TokenManager tm = TokenManager.getInstance();
        HashMap hm = new HashMap();
        if(tm.isValidToken(token)) {
            CategoryModel model = this.categorySrv.findById(id);
            if(model.getCatUser().getUsername().equals(tm.getCurrentUser(token))) {
                this.categorySrv.delete(id);
                hm.put("info", model.getCat_name() + " successfully deleted");
                return hm;
            } else {
                hm.put("info", "You can not delete another user\'s category"+tm.getCurrentUser(token)+" "+model.getCatUser().getUsername().toString());
                return hm;
            }
        } else {
            return null;
        }
    }

    @RequestMapping(
            method = {RequestMethod.GET}
    )
    @ResponseBody
    public List<CategoryModel> getAllCategory(HttpServletResponse response) {
        return this.categorySrv.findAll();
    }
}
