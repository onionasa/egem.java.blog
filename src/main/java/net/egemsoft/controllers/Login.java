package net.egemsoft.controllers;


import net.egemsoft.auth.MyAuthenticationManager;
import net.egemsoft.auth.TokenManager;
import net.egemsoft.models.TestModel;
import net.egemsoft.services.TestSrv;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.ws.rs.FormParam;

import java.util.HashMap;
import java.util.List;

/**
 * Created by orionasa on 22.07.2015.
 */

@Controller
@RequestMapping("/login")
public class Login {
    @Autowired
    private TestSrv testSrv;
    private static AuthenticationManager am = new MyAuthenticationManager();
    @RequestMapping(method = RequestMethod.POST)
    @ResponseBody
    public HashMap<String,Object> getTest(HttpServletRequest request, HttpServletResponse response) {
        try {
            Authentication rs = new UsernamePasswordAuthenticationToken(request.getParameter("username"), request.getParameter("password"));
            Authentication result = am.authenticate(rs);
            SecurityContextHolder.getContext().setAuthentication(result);
            TokenManager tm = TokenManager.getInstance();
            tm.setToken(request.getParameter("username"),request);
            HashMap<String,Object> hm = new HashMap<String,Object>();
            hm.put("Token",tm.getUserToken(request.getParameter("username")));
            return hm;
        } catch(AuthenticationException e) {
            System.out.println("Authentication failed: " + e.getMessage());
            HashMap<String,Object> hm = new HashMap<String,Object>();
            hm.put("ERROR","USERNOTFOUND");
            return hm;
        }

    }



}
