package net.egemsoft.controllers;


import net.egemsoft.models.UserModel;
import net.egemsoft.services.UserSrv;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by orionasa on 24.07.2015.
 */
@Controller
@RequestMapping("/register")
public class Register {
    @Autowired
    private UserSrv userSrv;

    @RequestMapping(method = RequestMethod.POST)
    @ResponseBody
    public HashMap<String, Object> createUser(@RequestBody Map<String, Object> request, HttpServletResponse response) {
        HashMap<String, Object> hm = new HashMap<String, Object>();
        if (request == null) {
            hm.put("info", "parameters can not be null");
            return hm;
        } else {
            UserModel userModel = new UserModel(
                    request.get("username").toString()
                    , request.get("password").toString()
                    , "ROLE_ADMIN"
                    , request.get("mail").toString()
            );

            userSrv.create(userModel);
            hm.put("info", "user successfully created " + userModel.getUsername());
            return hm;
        }

    }

}
