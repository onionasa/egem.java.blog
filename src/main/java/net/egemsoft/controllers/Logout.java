package net.egemsoft.controllers;

import net.egemsoft.auth.TokenManager;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.HashMap;

/**
 * Created by orionasa on 22.07.2015.
 */
@Controller
@RequestMapping("/userLogout")
public class Logout {

    @RequestMapping(method = RequestMethod.GET)
    @ResponseBody
    public HashMap<String,Object> getTest(@RequestHeader("TOK") String token,HttpServletResponse response) {
        try{
            TokenManager tm = TokenManager.getInstance();
            tm.removeToken(token);
            System.out.println(token+"TOKENNNNNNNNNNNNNNNNNNNNNNNNn");
            HashMap<String,Object> hm = new HashMap<String, Object>();
            hm.put("Info", "User Loged Out");
            response.setStatus(HttpServletResponse.SC_OK);
            return hm;
        }catch (Exception e){
            System.out.println(e+"ECCCCCCCCCCCc");
            HashMap<String,Object> hma = new HashMap<String, Object>();
            hma.put("Info","User Loged Out");
            response.setStatus(HttpServletResponse.SC_OK);
            return hma;
        }

    }
}
