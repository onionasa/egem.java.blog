package net.egemsoft.controllers;


import net.egemsoft.auth.MyAuthenticationManager;
import net.egemsoft.auth.TokenManager;
import net.egemsoft.models.RelTestModel;
import net.egemsoft.models.TestModel;
import net.egemsoft.services.RelSrv;
import net.egemsoft.services.TestSrv;

import org.apache.commons.codec.binary.Base64;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.nio.charset.StandardCharsets;
import java.security.Principal;
import java.util.HashMap;
import java.util.List;

/**
 * Created by orionasa on 22.07.2015.
 */

@Controller
@RequestMapping("/test")
public class TestController {
    private static AuthenticationManager am = new MyAuthenticationManager();
    @Autowired
    private TestSrv testSrv;
    @Autowired
    private RelSrv relSrv;
    @RequestMapping( method = RequestMethod.GET)
    @ResponseBody
    public Object getTest(@RequestHeader("TOK") String token,HttpServletRequest request, HttpServletResponse response,Principal principal) {

        if(token==null){
            return "TOKEN NEEDED";
        }else{
        TokenManager tm = TokenManager.getInstance();
        if(tm.isValidToken(token)){
            return "LOGED USER "+tm.getCurrentUser(token);
        }else{
            return "NOTLOGED";
        }}



    }

}