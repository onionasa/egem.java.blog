# README #

### How do I get set up? ###

* Change dataSource settings
* mvn install
* mvn jetty:run

### API
 ###

* localhost:8080/api/register (POST) username/password/mail
* localhost:8080/api/login (POST) username/password
* localhost:8080/api/userLogout (GET) (-H "TOK:{token}")
* localhost:8080/api/category (GET,POST) cat_name (-H "TOK:{token}")
* localhost:8080/api/category/catList (GET) (-H "TOK:{token}")
* localhost:8080/api/category/deleteCat/{id} (GET) (-H "TOK:{token}")